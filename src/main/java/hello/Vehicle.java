package hello;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Vehicle{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long vehicleId;

    private int manufacturerId;
    private int modelId;
    private int typeId;

    private String motorcode;
    private String[] kba;

    public long getVehicleId() {
        return vehicleId;
    }
    public void setVehicleId(long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public int getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(int manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getMotorcode() {
        return motorcode;
    }

    public void setMotorcode(String motorcode) {
        this.motorcode = motorcode;
    }

    public String[] getKba() {
        return kba;
    }

    public void setKba(String[] kba) {
        this.kba = kba;
    }
}
